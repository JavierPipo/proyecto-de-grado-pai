<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|

*/
/*
 * Maladys
 * */
Route::resource('maladies','malady\maladyController',['except' => ['create', 'edit']]);
Route::resource('maladies.symptoms','malady\MaladySintomasController',['only' => ['index']]);
Route::resource('maladies.images','malady\MaladyImgController',['only' => ['index']]);
/*Route::get('test', function (){
    phpinfo();
});*/
/*
 * users
 * */
Route::get('users/me','user\userController@me');
Route::resource('users','user\userController',['except' => ['create', 'edit']]);
Route::resource('users.aportes','user\UserAportesController',['only' => ['index', 'store']]);

/*
 * Auth
 * */
Route::post('oauth/token','\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken');

/*
 * Aportes
 * */
Route::get('aportes/positions','aporte\AporteController@getAllPosition');
Route::resource('aportes','aporte\AporteController',['except' => ['create', 'edit']]);
Route::resource('aportes.relationships','aporte\AporteParentescoController',['only' => ['index']]);
Route::resource('aportes.cities','aporte\AporteCityController',['only' => ['index']]);
Route::resource('aportes.symptoms','aporte\AporteSintomasController',['only' => ['index']]);

/*
 * relationship
 * */
Route::resource('relationships','parentesco\ParentescoController',['except' => ['create', 'edit']]);

/*
 * Symptoms
 * */

Route::resource('symptoms','sintomas\SintomasController', ['except' => ['create', 'edit']]);