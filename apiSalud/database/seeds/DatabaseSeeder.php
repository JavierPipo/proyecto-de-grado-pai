<?php

use App\models\Aporte;
use App\models\Ciudad;
use App\models\Departamento;
use App\models\Img;
use App\models\Malady;
use App\models\Parentesco;
use App\models\Rol;
use App\models\Sintomas;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');//desactivar verificacion de llaves foraneas.

        Malady::truncate();
        Aporte::truncate();
        User::truncate();
        Ciudad::truncate();
        Sintomas::truncate();
        Departamento::truncate();
        Parentesco::truncate();
        Rol::truncate();
        Img::truncate();

        DB::table('img_malady');
        DB::table('aporte_sintoma');
        DB::table('malady_sintoma');
        DB::table('aporte_malady');


        $cantidadDeUsuarios = 30;
        $cantidadDeEnfermedades = 40;
        $cantidadDeAportes = 40;
        $cantidadDeCiudades = 20;
        $cantidadDeSintomas = 80;
        $cantidadDeDepartamentos = 3;
        $cantidadDeParentescos = 20;
        $cantidadDeRoles = 2;
        $cantidadDeImg = 2;

        factory(Img::class, $cantidadDeImg)->create();
        factory(Rol::class, $cantidadDeRoles)->create();
        factory(Parentesco::class, $cantidadDeParentescos)->create();
        factory(Departamento::class, $cantidadDeDepartamentos)->create();
        factory(Sintomas::class, $cantidadDeSintomas)->create();
        factory(Ciudad::class, $cantidadDeCiudades)->create();
        factory(User::class, $cantidadDeUsuarios)->create();

        factory(Malady::class, $cantidadDeEnfermedades)->create()->each(
            function ($malady){
                $sintoma = Sintomas::all()->random(mt_rand(1,8))->pluck('id');
                $img = Img::all()->random(mt_rand(0,2))->pluck('id');

                $malady->symptom()->attach($sintoma);
                $malady->img()->attach($img);
            }
        );

        factory(Aporte::class, $cantidadDeAportes)->create()->each(
            function ($aporte){
                $sintomas = Sintomas::all()->random(mt_rand(1,8))->pluck('id');
                $maladies = Malady::all()->random(mt_rand(0,2))->pluck('id');


                $aporte->symptom()->attach($sintomas);
                $aporte->malady()->attach($maladies);
            }
        );

    }
}
