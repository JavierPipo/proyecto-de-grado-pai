<?php

use App\models\Aporte;
use App\models\Ciudad;
use App\models\Departamento;
use App\models\Img;
use App\models\Malady;
use App\models\Parentesco;
use App\models\Rol;
use App\models\Sintomas;
use App\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(Rol::class, function (Faker $faker) {
    return [
        'name' =>$faker->randomElement(['Administrador','regular']),
    ];
});

$factory->define(Img::class, function (Faker $faker) {
    return [
        'name' => $faker->image('/tmp', '640','480'),
    ];
});

$factory->define(Sintomas::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->paragraph(1),
    ];
});

$factory->define(Departamento::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement(['Cundinamarca','Boyaca','Antioquia']),
    ];
});

$factory->define(Ciudad::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'department_id' => Departamento::all()->random()->id,
    ];
});

$factory->define(Malady::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->paragraph(1),
    ];
});

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'last_names' => $faker->lastName,
        'date_born' => $faker->date('Y-m-d', 'now'),
        'rol_id' => Rol::all()->random()->id,
        'img_id' => Img::all()->random()->id,
    ];
});

$factory->define(Parentesco::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement(['mi usuario','padre','madre','hijo','hija','cónyuge','yerno','nuera','suegro','suegra','abuelo','abuela','hermano','hermana','Tío','Tía','sobrino','sobrina','Primo','Prima']),
    ];
});

$factory->define(Aporte::class, function (Faker $faker) {
    return [
        'name_person' => $faker->name,
        'last_name_person' => $faker->lastName,
        'date_born' => $faker->date('Y-m-d', 'now'),
        'loc_longitude' => $faker->longitude,
        'loc_latitude' => $faker->latitude,
        'user_id' => User::all()->random()->id,
        'relationship_id' => Parentesco::all()->random()->id,
        'city_id' => Ciudad::all()->random()->id,
    ];
});


