<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AporteSintomaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aporte_sintomas', function (Blueprint $table) {
            $table->integer('aporte_id')->unsigned();
            $table->integer('sintomas_id')->unsigned();

            $table->foreign('aporte_id')->references('id')->on('aportes');
            $table->foreign('sintomas_id')->references('id')->on('sintomas');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aporte_sintoma');
    }
}
