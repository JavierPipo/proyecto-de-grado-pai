<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MaladySintomaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('malady_sintomas', function (Blueprint $table) {
            $table->integer('malady_id')->unsigned();
            $table->integer('sintomas_id')->unsigned();

            $table->foreign('malady_id')->references('id')->on('maladies');
            $table->foreign('sintomas_id')->references('id')->on('sintomas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('malady_sintoma');
    }
}
