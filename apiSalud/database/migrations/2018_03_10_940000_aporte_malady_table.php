<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AporteMaladyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aporte_malady', function (Blueprint $table) {
            $table->integer('malady_id')->unsigned();
            $table->integer('aporte_id')->unsigned();

            $table->foreign('malady_id')->references('id')->on('maladies');
            $table->foreign('aporte_id')->references('id')->on('aportes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aporte_malady');
    }
}
