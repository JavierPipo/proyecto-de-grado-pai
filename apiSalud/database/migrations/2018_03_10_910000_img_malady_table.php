<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImgMaladyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('img_malady', function (Blueprint $table) {
            $table->integer('img_id')->unsigned();
            $table->integer('malady_id')->unsigned();

            $table->foreign('img_id')->references('id')->on('imgs');
            $table->foreign('malady_id')->references('id')->on('maladies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('img_malady');
    }
}
