<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\apiController\ApiController;
use App\models\Aporte;
use App\models\Ciudad;
use App\models\Departamento;
use App\models\Sintomas;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use function MongoDB\BSON\toJSON;
use Psy\Util\Json;

class UserAportesController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $aportes = $user->aportes()
            ->with('city', 'relationship')
            ->get();
        return $this->showAll($aportes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $campos = $request->all();

        $rules = [
            'name_person' => 'required',
            'last_name_person' => 'required',
            'date_born' => 'required',
            'city_id' => 'required',
            'loc_latitude' => 'required',
            'loc_longitude' => 'required',
            'relationship_id' => 'required',
        ];

        $this->validate($request, $rules);

        $arrDate = explode(',', $campos['city_id']);

        $city = Ciudad::where('name', '=', trim($arrDate[0]))->get();
        $department = Departamento::where('name', '=', trim($arrDate[1]))->get();

        if(count($city) > 0)
        {
            $campos['city_id'] = $city[0]->id;
        }
        else if(count($department) > 0)
        {
            $campos['city_id'] = $this->_fnCreateCity(trim($arrDate[0]), $department[0]->id)->id;
        }
        else
        {
            $newDepartment = new Departamento();
            $newDepartment->name = trim($arrDate[1]);
            $newDepartment->save();

            $campos['city_id'] = $this->_fnCreateCity(trim($arrDate[0]), $newDepartment->id)->id;
        }

        $arrSymptoms = json_decode($campos['arrSymptoms']);

        unset($campos['arrSymptoms']);
        $campos['relationship_id'] = intval($campos['relationship_id']);
        $campos['date_born'] = Carbon::parse($campos['date_born'])->format('Y-m-d');
        $campos['user_id'] = $user->id;

        $aporte = Aporte::create($campos);
        
        foreach ($arrSymptoms as $key => $value ) {
            //dd($value->id);
            $sintoma = Sintomas::find($value->id);
            $aporte->symptom()->attach($sintoma);
        }
        return $this->showOne($aporte);
    }

    private function _fnCreateCity($name, $departmentId) {
        $newCity = new Ciudad();
        $newCity->name = $name;
        $newCity->department_id = $departmentId;
        $newCity->save();
        return $newCity;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
