<?php

namespace App\Http\Controllers\aporte;

use App\Http\Controllers\apiController\ApiController;
use App\models\Aporte;
use App\User;
use Illuminate\Http\Request;

class AporteController extends ApiController
{
    public function __construct()
    {
        $this->middleware('client.credentials')->only(['getAllPosition']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Aporte $aporte)
    {
        return $this->showOne($aporte);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAllPosition()
    {
        //dd(Aporte::all());
        return $this->showAll(Aporte::all());
    }
}
