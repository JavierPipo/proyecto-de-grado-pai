<?php

namespace App\Http\Controllers\malady;

use App\Http\Controllers\apiController\ApiController;
use App\models\Img;
use App\models\Malady;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MaladyImgController extends ApiController
{
    public function __construct()
    {
        $this->middleware('client.credentials')->only(['index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Malady $malady)
    {
        $images = $malady->img;

        return $this->showAll($images);
    }
}
