<?php

namespace App\Http\Controllers\malady;

use App\Http\Controllers\apiController\ApiController;
use App\models\Malady;
use Illuminate\Http\Request;

class maladyController extends ApiController
{
    public function __construct()
    {
        $this->middleware('client.credentials')->only(['index','show', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arrMaladyModel = Malady::with('img:name')->get();

        return $this->showAll($arrMaladyModel);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Malady $malady)
    {
        return $this->showOne($malady);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Malady $malady)
    {
        $malady->delete();
        return $this->showOne($malady) ;
    }
}
