<?php

namespace App;

use App\models\Aporte;
use App\models\Img;
use App\models\Rol;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes ,HasApiTokens;
    const user_deleted = 1;
    const user_no_deleted= 0;

    protected $dates = ['deleted_at'] ;
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'deleted',
        'last_names',
        'date_born',
        'rol_id',
        'img_id'
    ];

    /*
     * creacion de mutadores: los mutadores se ejecutan antes de guardar los atributos en la BD
     *
     * */
    public function setNameAttribute($valor)
    {
        $this->attributes['name'] = strtolower($valor);
    }

    public function setEmailAttribute($valor)
    {
        $this->attributes['email'] = strtolower($valor);
    }

    /*
     * creacion de accesores: los accesores se ejecutan cuando se obtiene los atributos de la BD
     *
     * */
    public function getNameAttribute($valor)
    {
        return ucwords($valor);
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //relations
    public function rol()
    {
        return $this->belongsTo(Rol::class);
    }

    public function img()
    {
        return $this->belongsTo(Img::class);
    }

    public function aportes()
    {
        return $this->hasMany(Aporte::class);
    }
}
