<?php

namespace App\models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Aporte extends Model
{
    use SoftDeletes;

    protected $table = 'aportes';
    protected $dates = ['deleted_at'] ;

    protected $fillable = [
        'name_person',
        'last_name_person',
        'date_born',
        'loc_longitude',
        'loc_latitude',
        'user_id',
        'relationship_id',
        'city_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function relationship()
    {
        return $this->belongsTo(Parentesco::class);
    }

    public function city()
    {
        return $this->belongsTo(Ciudad::class);
    }

    public function symptom()
    {
        return $this->belongsToMany(Sintomas::class);
    }

    public function malady()
    {
        return $this->belongsToMany(Malady::class);
    }
}
