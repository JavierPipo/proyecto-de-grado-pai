<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Malady extends Model
{
    Use SoftDeletes;

    protected $table = 'maladies';
    protected $dates = ['deleted_at'] ;
    protected $hidden = [
        'pivot'
    ];
    protected $fillable = [
        'name',
        'description'
    ];

    /*
     * mutador para el nombre
     * */
    public function setNameAttribute($valor)
    {
        $this->attributes['name'] = strtolower($valor);
    }

    /*
     * accesor para nombre y parrafo
     *
     * */
    public function getNameAttribute($valor)
    {
        return ucfirst($valor);
    }

    public function getDescriptionAttribute($valor)
    {
        return ucfirst($valor);
    }

    //relations
    public function img()
    {
        return $this->belongsToMany(Img::class);
    }

    public function symptom()
    {
        return $this->belongsToMany(Sintomas::class);
    }

    public function aporte()
    {
        return $this->belongsToMany(Aporte::class);
    }
}
