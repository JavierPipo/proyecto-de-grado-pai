<?php

namespace App\models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Img extends Model
{
    use SoftDeletes;

    protected $table = 'imgs';
    protected $dates = ['deleted_at'] ;
    protected $hidden = [
        'pivot'
    ];
    protected $fillable = [
        'description'
    ];

    public function user()
    {
        return $this->hasMany(User::class);
    }

    public function malady()
    {
        return $this->belongsToMany(Malady::class);
    }
}
