<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Departamento extends Model
{
    use SoftDeletes;

    protected $table = 'departments';
    protected $dates = ['deleted_at'] ;

    protected $fillable = [
        'name',
    ];

    public function city()
    {
        return $this->hasMany(Ciudad::class);
    }
}
