<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parentesco extends Model
{
    use SoftDeletes;

    protected $table = 'relationships';
    protected $dates = ['deleted_at'] ;

    protected $fillable = [
        'name'
    ];

    public function user()
    {
        return $this->hasMany(Aporte::class);
    }
}
