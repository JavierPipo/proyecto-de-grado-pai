<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ciudad extends Model
{
    use SoftDeletes;

    protected $table = 'cities';
    protected $dates = ['deleted_at'] ;

    protected $fillable = [
        'name',
        'department_id',
    ];

    public function department()
    {
        return $this->belongsTo(Departamento::class);
    }

    public function aportes()
    {
        return $this->hasMany(Aporte::class);
    }
}
