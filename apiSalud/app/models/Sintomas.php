<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sintomas extends Model
{
    use SoftDeletes;

    protected $table = 'sintomas';
    protected $dates = ['deleted_at'] ;
    protected $hidden = [
        'pivot'
    ];
    protected $fillable = [
        'name',
        'description',
    ];

    public function aporte()
    {
        return $this->belongsToMany(Aporte::class);
    }

    public function maladys()
    {
        return $this->belongsToMany(Malady::class);
    }
}
